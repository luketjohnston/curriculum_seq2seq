
-- Creates the models, the encoder and decoder, and clones them many times.
-- Cloning is necessary for backpropogation - can't forward something through a
-- model multiple times and then backpropogate multiple times  - it will have
-- forgotten the relevant data from the first time. The solution is to make a
-- clone of the model for each element of the sequence, forwarding and
-- backwarding each element of the sequence through its own clone. All clones
-- will share the same parameters, so the backward pass will update the
-- parameters for all clones.


require 'torch'   -- torch
require 'image'   -- for image transforms
require 'nn'      -- provides all sorts of trainable modules/layers
require 'rnn'
require 'read_options.lua'
model_utils = require 'model_utils'
local LSTM = require 'LSTM.lua'

if not opt then
  opt = getOptions()
end
if opt.useCuda == 1 then require 'cunn' end


----------------------------------------------------------------------
print '==> define parameters'
-- Size of the output of the recurrent layer
hiddenSize = 400
num_layers = 2
if opt.fastTest == 1 then
  hiddenSize = 50
  num_layers = 1
end

----------------------------------------------------------------------
print '==> construct model'

encoder = LSTM.lstm(vocab.size, hiddenSize, num_layers, 0.5)
if opt.useCuda == 1 then encoder:cuda() end

decoder = LSTM.lstm(vocab.size, hiddenSize, num_layers, 0.5)
if opt.useCuda==1 then decoder:cuda() end

  
-- initialize forget gates to 1
for layer_idx = 1, num_layers do
  for _,node in ipairs(encoder.forwardnodes) do
    if node.data.annotations.name == "i2h_" .. layer_idx then
      print('setting forget gate biases to 1 in encoder layer ' .. layer_idx)
      -- the gates are, in order, i,f,o,g, so f is the 2nd block of weights
      node.data.module.bias[{{hiddenSize+1, 2*hiddenSize}}]:fill(1.0)
    end
  end
  for _,node in ipairs(decoder.forwardnodes) do
    if node.data.annotations.name == "i2h_" .. layer_idx then
      print('setting forget gate biases to 1 in decoder layer ' .. layer_idx)
      -- the gates are, in order, i,f,o,g, so f is the 2nd block of weights
      node.data.module.bias[{{hiddenSize+1, 2*hiddenSize}}]:fill(1.0)
    end
  end
end

-- need to set the parameters of both models into one tensor, for simultaneous optimization.
fullmodel = encoder:add(decoder)
parameters,gradParameters = fullmodel:getParameters()

parameters:uniform(-0.08,0.08) -- small uniform initialization


-- clone model so one model for each forward/backward pass
enc_clones = {}; dec_clones = {}
for i=1,opt.minLen+3+2 do -- 3 from 4_train, 2 from start and finish indicators.
  enc_clones[i] = model_utils.clone(encoder)
  dec_clones[i] = model_utils.clone(decoder)
end

--enc_clones = model_utils.clone_many_times(encoder,data.largest_seq)
--dec_clones = model_utils.clone_many_times(decoder,data.largest_seq)

----------------------------------------------------------------------
print('The encoder:')
print(encoder)
print('The decoder:')
print(decoder)

----------------------------------------------------------------------

