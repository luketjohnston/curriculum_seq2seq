-- Define the loss function.

require 'torch'   -- torch
require 'nn'      -- provides all sorts of loss functions

---------------------------------------------------------------------

criterion = nn.ClassNLLCriterion()
if opt.useCuda == 1 then criterion:cuda() end


----------------------------------------------------------------------
print '==> here is the loss function:'
print(criterion)
