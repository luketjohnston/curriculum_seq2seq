

-- this function forwards the input through the network, and returns the
-- normalized loss. it updates and normalizes gradparameters in the process.
--
-- should have already updated the parameters for the model from last
-- optimization result, and should have already zeroed gradients from last
-- optimization result.
--
-- inputs and targets are both tables of input tensors, formed from the
-- makebatch() function in minibatch.lua
evalBatch = function(criterion, inputs, targets)

  batch_size = (#(inputs[1]))[1] -- may be diff than opt.batchSize if near end of data

  -- initial rnn state will be all zeros. Also used as initial dl_dstate
  init_state = {}
  for i=1,num_layers do
    local h_init = torch.zeros(batch_size,hiddenSize)
    if opt.useCuda == 1 then h_init = h_init:cuda() end
    table.insert(init_state,h_init:clone()) -- one for h
    table.insert(init_state,h_init:clone()) -- one for c
  end


  -- the ith entry records the enc_state before char i
  enc_state = {}
  -- will record all outputs of the encoder
  enc_outs = {}
  -- setup first encoder input
  table.insert(enc_state, init_state)
  local i
  for i=1,#inputs do
    enc_outs[i] = enc_clones[i]:forward({inputs[i],unpack(enc_state[i])})
    -- update next enc_state[i+1]
    enc_state[i+1] = {}
    for l=1,#init_state do
      table.insert(enc_state[i+1],enc_outs[i][l])
    end
  end

  -- will record all inputs to the decoding step
  -- the first hidden inputs are the hidden state of the last step of encoding
  dec_state = {enc_state[#enc_state]}
  -- will record all outputs from the decoding step
  dec_outs = {} 
  for i=1,(#targets)-1 do 
    dec_outs[i] = dec_clones[i]:forward({targets[i], unpack(dec_state[i])})
    -- update next dec_state[i+1]
    dec_state[i+1] = {}
    for l=1,#init_state do
      table.insert(dec_state[i+1],dec_outs[i][l])
    end
  end      

  -- this will hold dloss_dstate for the rest of the layers
  dl_dstate = {}
  for i=1,#init_state do dl_dstate[i] = init_state[i]:clone() end

  local loss = 0
  local dl_do
  for i=(#targets)-1,1,-1 do
    -- calc loss
    log_probs = dec_outs[i][#(dec_outs[i])] -- log probs for ith iteration
    loss = loss + criterion:forward(log_probs, targets[i+1])
    -- calc dloss_doutput (for the softmax layer)
    dl_do = criterion:backward(log_probs, targets[i+1])
    -- backward pass for decoding step i
    table.insert(dl_dstate,dl_do:clone())
    local back = dec_clones[i]:backward({targets[i],unpack(dec_state[i])},dl_dstate)
    dl_dstate = {unpack(back,2)} -- throws out the dl_din, which is unneeded
  end

  -- for the rest of the backward passes, dl_do = 0
  dl_do:zero()

  for i=(#inputs),1,-1 do
    table.insert(dl_dstate,dl_do)
    local back = enc_clones[i]:backward({inputs[i],unpack(enc_state[i])},dl_dstate)
    dl_dstate = {unpack(back,2)} -- throws out the dl_din, which is unneeded
  end
    
  gradParameters:clamp(-opt.gradClip, opt.gradClip)
  loss = loss / (#inputs) / batch_size

  return loss, gradParameters
end 


