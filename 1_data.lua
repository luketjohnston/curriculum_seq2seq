-------------------------------------------------------------------------------
-- loads opt.data into torch tensor and saves to file. Will load 
-- from this second file on subsequent calls.
-- IMPORTANT NOTE: right now, this doesn't load any data at all - it just saves
-- a bunch of random numbers as the data. I want to test and make sure the model
-- can memorize random numbers before I use it to memorize text.
-- I added "TODO" flags to the parts where I changed it to use random numbers
-- instead of loading data from file.
-------------------------------------------------------------------------------

require 'torch'   -- torch
require 'image'   -- for color transforms
require 'nn'      -- provides a normalization operator


----------------------------------------------------------------------
print '==> loading dataset'

-- We load the dataset from disk, and re-arrange it to be compatible
-- with Torch's representation. Matlab uses a column-major representation,
-- Torch is row-major, so we just have to transpose the data.

-- Note: the data, in X, is 4-d: the 1st dim indexes the samples, the 2nd
-- dim indexes the color channels (RGB), and the last two dims index the
-- height and width of the samples.

-- filenames of torch tensors of data and vocab
data_file = 'data/data.t7'
vocab_file = 'data/vocab.t7'

-- the period character, to mark the end of sequences.
delimiter_char = '\n'
-- starting minimum length of sequences to learn.
min_len = opt.minLen

if paths.filep(vocab_file) then
  data = torch.load(data_file)
  vocab = torch.load(vocab_file)
else
  -- create tensor for data and vocab and save to file
  -- mappings from characters to ints and vica versa
  vocab = {char_to_int = {}, int_to_char = {}, size = 0}

  f = io.open(opt.data, "r")
  -- build vocab and determine length of data
  local cache_len = 10000 -- # of characters to read in at one time
  local total_characters = 0 -- total characters in whole dataset
  rawdata = f:read(cache_len) -- read line of data (one email)
  last_del = 0; char_count = 0
  repeat
    total_characters = total_characters + rawdata:len()
    for char in rawdata:gmatch'.' do
      char_count = char_count + 1
      -- update vocab if necessary    
      if not vocab.char_to_int[char] then 
        vocab.size = vocab.size + 1
        vocab.char_to_int[char] = vocab.size 
        vocab.int_to_char[vocab.size] = char
      end
    end
    rawdata = f:read(cache_len)
  until not rawdata
  f:close()

  -- determine end of training data (and beginning of validation data)
  local training_chars = math.floor((1-opt.val) * total_characters)
  --local training_chars = training_chars - (training_chars % (opt.seqLength * opt.batchSize)) -- round to batch size


  -- for random TODO
  vocab.char_to_int = {}
  vocab.int_to_char = {}
  vocab.size = 11
  for i=0,9 do
    vocab.char_to_int['' .. i] = 9-i+1
    vocab.int_to_char[9-i+1] = '' .. i
  end
  vocab.char_to_int['X'] = 11
  vocab.int_to_char[11] = 'X'
  vocab.end_char_ind = 11



  -- construct data tensor
  inputs = {} 
  targets = {}
  f = io.open(opt.data,"r")
  rawdata = f:read(cache_len)
  local c = 1
  -- Tensor to hold all character inputs.
  train = torch.ByteTensor(training_chars) 
  val = torch.ByteTensor(total_characters - training_chars) 
  repeat
    for i=1,#rawdata do
      if c <= training_chars then
        -- TODO changed
        -- train[c] = vocab.char_to_int[rawdata:sub(i,i)] 
        train[c] = vocab.char_to_int['' .. math.random(0,9)]
      elseif c <= total_characters then
        -- TODO changed
        -- val[c-training_chars] = vocab.char_to_int[rawdata:sub(i,i)]
        val[c-training_chars] = vocab.char_to_int['' .. math.random(0,9)]
      end
      c = c + 1
    end
    rawdata = f:read(cache_len)
  until not rawdata
  f:close()

  print("Length of training data: " .. (#train)[1])
  print("Length of validation data: " .. (#val)[1])




  -- now save vocab and data tensors to file
  torch.save(vocab_file, vocab)
  data = {
    train = train,
    val = val,
    --delimiter = vocab.char_to_int[delimiter_char] TODO
  }
  torch.save(data_file, data) 
end


--delimiter = vocab.char_to_int[delimiter_char] TODO



