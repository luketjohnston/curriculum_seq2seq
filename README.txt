This is a sequence-to-sequence learner.

It is implemented as 2 lstms: the encoder, and the decoder.

The encoder reads in a sequence, and then passes its hidden state to the
decoder. The decoder's output is a probability distribution over the alphabet,
which is then sampled from to generate the output sequence. The last sampled
character is passed as input to the decoder.

Eventually, I want this model to memorize textual data, but right now it just
memorizes sequences of random numbers. 

It uses a very simple curriculum learning strategy. min_len in 4_train.lua keeps
track of a minimum length of sequence that the model needs to work on
memorizing. Before the model is trained on a sequence, a random sequence length
from min_len to min_len + 3 is chosen. Then, a random sequence of numbers is
generated with this length, and the model is trained on that random sequence of
numbers. After the model is doing sufficiently well with the current min_len,
min_len is increased by 3. This strategy is inspired by the "learning to execute" 
papers results, although it is different than the strategies they used. 


The target sequence has a special element added to its beginning and end - 'X'.
This element is forwarded through the decoder to get the decoder's prediction
for the first element of the sequence, and this element is also used to denote
the end of a sequence. That is, if the decoder predicts element 'X' (when
sampling), then the decoding is finished.


Run with 
th doall.lua


See read_options.lua for options. I.e.

th doall.lua -learningRate 1e-4 -batchSize 50 -minLen 10

will run with a learning rate of 1e-4 and batch size of 50, and will start the
model training on sequences of length 10-13 (rather than 3-6). Idk how other
settings will work, I haven't done much experimentation with the latest version
of the code.
