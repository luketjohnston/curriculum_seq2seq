

-- given torch tensors of inputs and targets, a start point, and batch size,
-- returns the table of inputs and the table of targets for that minibatch.
-- use_cpu is optional, if not specified, uses CUDA.
--
-- Requires that the inputs and targets have already been processed to be a
-- multiple of (batch_size * seq_len)
makebatch = function(inputs,targets,start,seq_len,batch_size)

  -- create mini batch
  -- We do batch processing so that we can update the parameters less
  -- frequently, and the updates are more effective (less noise) => faster.
  local batch_inputs = {}
  -- targets will be same sequences as inputs, but with a special "start"
  -- character prepended.
  local batch_targets = {}



  -- if we're near the end of the data, may need to decrease seq_len or
  -- batch_size
  seq_len = math.min((#data.train)[1] - start, seq_len) 
  while start + batch_size * seq_len - 1 > (#data.train)[1] do
    batch_size = batch_size - 1
  end

  -- This table will be used to index one character from each sequence at a
  -- time, to form a single batch.
  local offsets = torch.LongTensor(batch_size)
  for i=1,batch_size do offsets[i] = start + (i-1)*seq_len end

  -- insert "end" characters in the beginning of the sequences
  ends = torch.ByteTensor(batch_size)
  ends:fill(vocab.end_char_ind)
  if opt.useCuda == 1 then ends = ends:cuda() end
  table.insert(batch_targets,ends)
  table.insert(batch_inputs,ends)


  for i=1,seq_len do
    input = inputs:index(1,offsets) -- get batch of inputs
    target = targets:index(1,offsets) -- corresponding batch of targets
    if opt.useCuda == 1 then
      target = target:cuda()
    end 
    table.insert(batch_inputs, input) -- insert batch into sequencer input table
    table.insert(batch_targets, target)
    offsets:add(1) -- increment offsets so we index the next batch of chars.
  end

  -- insert "end" characters in the end of the target sequences
  table.insert(batch_targets,ends)
  table.insert(batch_inputs,ends)

  return batch_inputs,batch_targets
end


-- takes one of the outputs of the above makebatch function, and returns the
-- corresponding string. Assumes that "makebatch" was called with batch_size =
-- 1.
batch_to_string = function(inputs)
  s = ''
  for i=1,#inputs do
    s = s .. vocab.int_to_char[inputs[i][1]]
  end
  return s
end
