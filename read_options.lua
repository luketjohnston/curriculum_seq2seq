-- File to read command line options
--

require 'torch'

getOptions = function()

  cmd = torch.CmdLine()
  cmd:text()
  cmd:text('Options:')
  -- global:
  cmd:option('-seed', 1, 'fixed input seed for repeatable experiments')
  cmd:option('-threads', 2, 'number of threads')
  cmd:option('-useCuda',1,'Set to 0 if you want to run on CPU.')
  -- filenames:
  cmd:option('-save', '/home/luke/Research/saves/seq2seq/minibatch2', 'subdirectory to save/log experiments in')
  cmd:option('-model','nil','Filename of saved model')
  --cmd:option('-data','/home/luke/Data/Books/books_large_p1.txt','Filename of the text datafile.')
  --cmd:option('-data','data/wordtest.txt','Filename of the text datafile.')
  cmd:option('-data','data/tinyshakespeare.txt','Filename of the text datafile.')
  -- preferences
  cmd:option('-valStep',500,'Number of batches in between validation calculations.')
  cmd:option('-temperature',1,'Temperature to sample the model with.')
  cmd:option('-val',0.002,'Proportion of the dataset to use for validation.')
  cmd:option('-fastTest',0,'Set to 1 to use a smaller model size for faster debugging.')
  -- training parameters:
  cmd:option('-learningRate', 1e-3, 'learning rate at t=0')
  cmd:option('-learningDecay',1.0, 'Decay learning rate by this factor whenever stuck. If 1.0, no decay.')
  cmd:option('-dropout',0.5,'Dropout')
  cmd:option('-batchSize',1,'Number of sequences to train on in parallel.')
  cmd:option('-gradClip',1,'Clip gradient magnitude below this value.') -- this should be <= 1 right? to prevent exploding gradients?
  cmd:option('-minLen',3,'Starting length of things to memorize')
  cmd:text()
  opt = cmd:parse(arg or {})
  return opt
end
